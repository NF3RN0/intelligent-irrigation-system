# Intelligent Irrigation System

Intelligent irrigation system using moisture sensors for use in residential and commercial applications


## Dependencies

Python 3, pip, sqlite, nginx, nodejs, npm 

`sudo apt install python3 python3-pip sqlite3 nginx nodejs npm`


Python Libraries

`pip3 install websockets digi-xbee watchdog pretty_errors requests`


## Usage

Change the directory to app.

`cd app`

For inital setup create the database and new readings table, then start the server. All in one line.

`./main -c -s`

For development and testing it might be necssary to create random data for the database.

`./main -f`

All additional options.

```
usage: main [-h] [-a] [-b] [-c] [-d] [-f] [-l] [-m] [-r] [-s] [-x] [-t]

options:
  -h, --help            show this help message and exit
  -a, --automate        Automate Scheduler, initialize hub logic.
  -b, --build-client    Build Web Client.
  -c, --create-table    Create new readings table.
  -d, --development     Run in development mode.
  -f, --fake-readings   Insert random fake moisture readings data.
  -l, --run-lcd         shows lcd menu.
  -m, --shell-menu      Run the curses shell menu.
  -r, --real-time-data  Enable realtime data from sensors.
  -s, --start-server    Start server.
  -x, --xbee-network-init
                        Initialize Xbee network.
  -t, --test-xbee-sensor
                        Test Xbee sensor.
```

## License

Copyright (c) Dustin Martin, Andrew Shoda Jr. All rights reserved.

Licensed under the [MIT](LICENSE) license.
