# Local Xbee (Coordinator) setup
import time
import pretty_errors
import datetime
import logging
from datetime import timedelta
from pprint import pprint

from digi.xbee.devices import XBeeDevice
from digi.xbee.util import utils
from digi.xbee.models.status import NetworkDiscoveryStatus
from digi.xbee.devices import XBeeDevice
from digi.xbee.io import IOLine, IOMode
from digi.xbee.exception import TimeoutException

from common.readings import MoistureReadings

from common import common

# Get the current time
currentDateTime = datetime.datetime.now()

# For Raspberry Pi /dev/serial0 or /dev/ttyUSB0 for Groove Board
PORT = "/dev/serial0"

# TODO: Replace with the baud rate of your local module.
BAUD_RATE = 9600

# Set IO Line for device, AD3 is the Potentiometer on the Grove Board
IOLINE_IN = IOLine.DIO3_AD3

# Discovery Timeout
DISCOVERY_TIMEOUT = 15

# Send and Recieve Sync Timeout
SYNC_TIMEOUT = 15

# Instantiate the Hub Xbee Device
local_xbee = XBeeDevice(PORT, BAUD_RATE)
# Instantiate the network
xbee_network = local_xbee.get_network()


class XbeeTxRxTime(object):
    '''This is a hack. We should redefine the data type in menu.'''
    def __init__(self, zone, moisture):
        self.zone = zone
        self.moisture = moisture

xbee_tx_rx_time = []

def return_xbee_tx_rx_time(xbee_tx_rx_time = xbee_tx_rx_time):
    return xbee_tx_rx_time


def xbee_network_init():

    local_xbee.open()
    #local_xbee.set_sync_ops_timeout(SYNC_TIMEOUT)
    xbee_network.set_discovery_timeout(DISCOVERY_TIMEOUT)

    # Find devices on the xbee network
    xbee_network.start_discovery_process(deep=True, n_deep_scans=1)

    while xbee_network.is_discovery_running():
        time.sleep(0.5)

    common.XbeeDevicesList = xbee_network.get_devices()

    # Set all parameters for devices on the network
    for obj in common.XbeeDevicesList:
        logging.debug("Setting IO Parameters")        
        tx_start = time.time()
        set_remote_device_analog(xbee_network, str(obj._node_id))
        tx_end = time.time()
        time_stat = XbeeTxRxTime(obj._node_id, tx_end - tx_start)
        xbee_tx_rx_time.append(time_stat)
        

def xbee_network_close():

    if local_xbee is not None and local_xbee.is_open():
        local_xbee.close()


def get_device_parameters(device):
    '''Prints the parameters of a device.'''

    print("Cached parameters\n" + "-" * 50)
    print("64-bit address:   %s" % device.get_64bit_addr())
    print("16-bit address:   %s" % device.get_16bit_addr())
    print("Node Identifier:  %s" % device.get_node_id())
    print("Firmware version: %s" % utils.hex_to_string(device.get_firmware_version()))
    print("Hardware version: %s" % device.get_hardware_version().description)
    print("")
    print("Non-Cached parameters\n" + "-" * 50)
    print("PAN ID:           %s" % utils.hex_to_string(device.get_pan_id()))
    print("Dest address:     %s" % device.get_dest_address())
    print("Power level:      %s" % device.get_power_level().description)



def get_network_devices(xbee_network):
    '''Searches the network and prints the MAC Address of remote devices on the network, returns all remote devices as a network object'''

    xbee_network.set_discovery_timeout(DISCOVERY_TIMEOUT)
    xbee_network.clear()

    # Callback for discovered devices.
    def callback_device_discovered(remote):
        print("Device discovered: %s" % remote)

    # Callback for discovery finished.
    def callback_discovery_finished(status):
        if status == NetworkDiscoveryStatus.SUCCESS:
            print("Discovery process finished successfully.")
        else:
            print("There was an error discovering devices: %s" % status.description)

    xbee_network.add_device_discovered_callback(callback_device_discovered)
    xbee_network.add_discovery_process_finished_callback(callback_discovery_finished)
    xbee_network.start_discovery_process()

    print("Discovering remote XBee devices...")

    while xbee_network.is_discovery_running():
        time.sleep(0.1)

    nodesList = xbee_network.get_devices()

    return nodesList


def custom_iteration(sequence):
    iterator = iter(sequence)
    completed_iterating = False
    while not completed_iterating:
        try:
            new_dev = next(iterator).get_node_id()
            print(new_dev)

        except StopIteration:
            completed_iterating = True

def set_remote_device_analog(xbee_network, REMOTE_NODE_ID):
    '''Takes in a node id of a remote device and set a specific pin to analog'''
    # Instantiate a remote device
    remote_device = xbee_network.discover_device(REMOTE_NODE_ID)
    if remote_device is None:
        logging.debug('Could not find the remote device: ' +  str(REMOTE_NODE_ID))
        return remote_device
    try:
        remote_device.set_io_configuration(IOLINE_IN, IOMode.ADC)
    except TimeoutException:
        logging.debug('Timeout error setting IO configuration for Node: ' + str(REMOTE_NODE_ID))

    return remote_device

def get_moisture_reading(xbee_network, REMOTE_NODE_ID):
    '''Get moisture reading from xbee device.'''

    logging.debug("--------GETTING MOISTURE VALUE--------")
    
    remote_device = xbee_network.get_device_by_node_id(REMOTE_NODE_ID)

    try:
        analog_value = remote_device.get_adc_value(IOLINE_IN)
        #print("%s: %d" % (IOLINE_IN, analog_value))
        reading_data = MoistureReadings(currentDateTime, REMOTE_NODE_ID, analog_value)
    except TimeoutException:
        logging.debug('Timeout error getting remote ADC value from Node: ' + str(REMOTE_NODE_ID))
        reading_data = None
    return reading_data

def get_all_zone_readings():
    all_readings = []
    devicesList = common.XbeeDevicesList
    logging.debug('-----GET ALL ZONE READINGS-----')
    for obj in devicesList:
        logging.debug("Processing Node:" + obj._node_id)
        device_reading = get_moisture_reading(xbee_network, obj._node_id)
        if device_reading is None:
            logging.debug("NO DEVICE READING RETURNED")
        else:
            all_readings.append(device_reading)
    
    return all_readings
