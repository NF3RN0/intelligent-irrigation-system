import sys
import curses
import traceback
import time
import logging
import asyncio
import threading

from curses import panel
from common import common
from console_args.c_args import CONSOLE_ARGS as args

if not args.dev:
    from xbee import coordinator

LINENUMBER = 7

XPosition = 6
dataList  = []

stop_refresh = False

class CancelOtherTasks(Exception):
    pass

class Menu(object):
    def __init__(self, items, title, info_screen, stdscreen):
        self.window = stdscreen.subwin(0, 0)
        self.window.keypad(1)
        self.panel = panel.new_panel(self.window)
        self.panel.hide()
        panel.update_panels()

        self.info_screen = info_screen
        self.position = 0
        self.items = items
        self.title = title
        self.items.append(("Exit", "Exit"))


    def navigate(self, n):
        self.position += n
        if self.position < 0:
            self.position = 0
        elif self.position >= len(self.items):
            self.position = len(self.items) - 1

    def display(self):
        self.panel.top()
        self.panel.show()
        self.window.clear()
        self.window.border(0)
        self.window.refresh()
        LineNum = LINENUMBER
        global stop_refresh

        if self.info_screen:
            info_thread = threading.Thread(target=self.display_info, args=())
            logging.debug("INFO SCREEN THREAD STARTED")
            info_thread.start()
            LineNum = 14

        while True:
            self.window.refresh()
            curses.doupdate()

            self.window.addstr(5, 5, self.title, curses.A_UNDERLINE)

            for index, item in enumerate(self.items):
                if index == self.position:
                    mode = curses.A_REVERSE
                else:
                    mode = curses.A_NORMAL

                msg = "%s" % (item[0])
                self.window.addstr(LineNum + index, XPosition, msg, mode)

            key = self.window.getch()

            if key in [curses.KEY_ENTER, ord("\n")]:
                if self.position == len(self.items) - 1:
                    if self.info_screen:
                        stop_refresh = True
                        info_thread.join()
                    break
                else:
                    self.items[self.position][1]()

            elif key == curses.KEY_UP:
                self.navigate(-1)

            elif key == curses.KEY_DOWN:
                self.navigate(1)

        self.window.clear()
        self.window.border(0)
        self.window.refresh()
        self.panel.hide()
        panel.update_panels()
        curses.doupdate()
        stop_refresh = False 

    def display_info(self):

        global stop_refresh
        LineNumTop = 7
        data_list = self.info_screen
        self.window.addstr(LineNumTop, XPosition, 'ZONE' + '      ' + 'READING' + '      ' + 'STATUS', curses.color_pair(3))
        time.sleep(3) # Give the network some time to initialize
        while True:
            self.window.refresh()
            LineNumTop = 8
            if data_list:
                logging.debug("--------READING data_list--------")
                for obj in data_list[0]:
                    self.window.addstr(LineNumTop, XPosition, str(obj.zone), curses.A_NORMAL)
                    if obj.moisture >= 500:
                        reading_color = curses.color_pair(1)
                    else:
                        reading_color = curses.color_pair(2)
                    self.window.addstr(LineNumTop, XPosition + 3, '       ' + str(obj.moisture), reading_color)  
                    LineNumTop += 1
                LineNumTop = 8
                for obj2 in data_list[1]:
                    self.window.addstr(LineNumTop, XPosition + 16, '       ' + str(obj2.status), curses.A_NORMAL)
                    LineNumTop += 1
            else:
                self.window.addstr(LineNumTop, XPosition, 'No values Returned. Yet...', curses.color_pair(1))
    
            if stop_refresh == True:
                break


class ShellMenu(object):
    def __init__(self, stdscreen):
        self.screen = stdscreen
        curses.initscr()
        curses.curs_set(0)
        curses.start_color()
        curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
        curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)
        curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_WHITE)

        #readings_menu_items = [] # There is no reason to refresh, just wait for the data
        #readings_menu = Menu(readings_menu_items, "Zone Readings", common.real_time_moisture_readings, self.screen)

        zone_menu_items = []
        zone_menu = Menu(zone_menu_items, "Current Zone Status", [common.real_time_moisture_readings, common.zone_status], self.screen)


        if common.XbeeDevicesList:
            xbee_statistics_items = []
            xbee_statistics = Menu(xbee_statistics_items, "Xbee Network Statistics", coordinator.return_xbee_tx_rx_time, self.screen)
            main_menu_items = [
                ("Xbee Network Statistics", xbee_statistics.display),
                ("Start Web Server", ),
                ("Zone Status", zone_menu.display)
            ]
        else:
            if args.dev:
                main_menu_items = [
                    ("Start Web Server", ),
                    ("Zone Status", zone_menu.display)
                ]
                main_menu = Menu(main_menu_items,'Intellegent Irrigation System [DEVELOPMENT MODE]', False, self.screen)
            else:
                main_menu_items = [
                    ("Initalize Xbee Network", coordinator.xbee_network_init),
                    ("Zone Status", zone_menu.display),
                    ("Start Web Server", )
                ]
                main_menu = Menu(main_menu_items,'Intellegent Irrigation System', False, self.screen)
       
        main_menu.display()


def menu_setup():
    curses.wrapper(ShellMenu)
    logging.debug("Menu Closed.")
    raise CancelOtherTasks
