class IrrigationSchedule:
    """Class for Irrigation Schedule"""

    def __init__(self, zone, timeStart, timeEnd):
        self.zone = zone
        self.timeStart = timeStart
        self.timeEnd = timeEnd