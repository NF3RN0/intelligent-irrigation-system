import websockets
import curses
import json
import socket
import datetime
import time
import random
import logging
from random import randrange
from datetime import timedelta
import pretty_errors

from console_args.c_args import CONSOLE_ARGS as args
from common.readings import MoistureReadings
from db import database

 #To put database in memory for testing use :memory:
if args.test or args.client:
    logging.debug("--- No connection to database---")
else:
    conn = database.create_connection('db/readings_history.db')

d1 = datetime.datetime.strptime('7/1/2021 1:30 PM', '%m/%d/%Y %I:%M %p')
d2 = datetime.datetime.strptime('1/25/2022 4:50 AM', '%m/%d/%Y %I:%M %p')

DEV_IP_ADDR = "127.0.0.1"

class ZoneStatus:
    """Class for real time Zone Status"""
    def __init__(self, zone, status):
        self.zone = zone
        self.status = status

def init():
    global sch_start_time
    global sch_end_time
    global returnException
    global XbeeDevicesList 
    global real_time_moisture_readings
    global enable_real_time_data
    global zone_status
    sch_start_time = datetime.datetime(year=2021,month=12,day=10,hour=0,minute=0,second=0)
    sch_end_time = datetime.datetime(year=2021,month=12,day=10,hour=0,minute=0,second=0)
    returnException = None
    XbeeDevicesList = []
    real_time_moisture_readings = []
    enable_real_time_data = False
    zone_status = [ ZoneStatus(1, "OFF"), ZoneStatus(2, "OFF"), ZoneStatus(3, "OFF"), ZoneStatus(4, "OFF") ]
    

db_path = 'db/readings_history.db'

# Get the current time
currentDateTime = datetime.datetime.now()

#Get the local ip address of the machine (Central Hub)
def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except Exception:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP


def random_date(start, end):
    """
    This function will return a random datetime between two datetime 
    objects.
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)
    return start + timedelta(seconds=random_second)


def insert_fake_readings(conn):
    '''Insert fake readings into the database for testing.'''
    random_data_integer =  random.randint(10, 50)

    for x in range(0, random_data_integer):
        reading_data = MoistureReadings(random_date(d1, d2), random.randint(1, 4), random.randint(0, 999))
        database.insert_reading(conn, reading_data)

    print("Inserted fake readings into table readings.")

def date_handler(obj):
    '''Handler to correctly parse datetime format '''
    return obj.isoformat() if hasattr(obj, 'isoformat') else obj

def create_random_xbee_data():
    '''Creates fake xbee data for testing.'''
    random_data_list = []
    for x in range(1, 5):
        reading_data = MoistureReadings(random_date(d1, d2), x, random.randint(0,999))
        random_data_list.append(reading_data)
    return random_data_list
