class MoistureReadings:
    """Class for Moisture Readings"""


    def __init__(self, timeStamp, zone, moisture):
        self.timeStamp = timeStamp
        self.zone = zone
        self.moisture = moisture