import time
import board
import digitalio
import pwmio
import logging
import pretty_errors
import adafruit_character_lcd.character_lcd as characterlcd
import RPi.GPIO as GPIO

from hub import hub
from common import common
from xbee import coordinator
from console_args.c_args import CONSOLE_ARGS as args

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

# Setup Inputs
GPIO.setup(10, GPIO.IN, pull_up_down=GPIO.PUD_UP) # SW1
GPIO.setup(9, GPIO.IN, pull_up_down=GPIO.PUD_UP) # SW2
GPIO.setup(11, GPIO.IN, pull_up_down=GPIO.PUD_UP) # SW3
GPIO.setup(8, GPIO.IN, pull_up_down=GPIO.PUD_UP) # SW4



# Modify this if you have a different sized character LCD
lcd_columns = 20
lcd_rows = 4

# Raspberry Pi Pin Config:
lcd_rs = digitalio.DigitalInOut(board.D26)  # LCD pin 4
lcd_en = digitalio.DigitalInOut(board.D19)  # LCD pin 6
lcd_d7 = digitalio.DigitalInOut(board.D27)  # LCD pin 14
lcd_d6 = digitalio.DigitalInOut(board.D22)  # LCD pin 13
lcd_d5 = digitalio.DigitalInOut(board.D24)  # LCD pin 12
lcd_d4 = digitalio.DigitalInOut(board.D25)  # LCD pin 11
lcd_rw = digitalio.DigitalInOut(
    board.D4
)  # LCD pin 5.  Determines whether to read to or write from the display.
# Not necessary if only writing to the display. Used on shield.

red = pwmio.PWMOut(board.D21)
green = pwmio.PWMOut(board.D12)
blue = pwmio.PWMOut(board.D18)

# Initialize the LCD class
# The lcd_rw parameter is optional.  You can omit the line below if you're only
# writing to the display.
lcd = characterlcd.Character_LCD_RGB(
    lcd_rs,
    lcd_en,
    lcd_d4,
    lcd_d5,
    lcd_d6,
    lcd_d7,
    lcd_columns,
    lcd_rows,
    red,
    green,
    blue,
    lcd_rw,
)

RED = [100, 0, 0]
GREEN = [0, 100, 0]
BLUE = [0, 0, 100]
PURPLE = [50, 0, 50]
YELLOW = [100, 40, 0]
ORANGE = [100, 5, 0]

def buttonpressed(channel):
    print('Pressed Button: ' + str(channel))
    global button_return

    button_return = channel


GPIO.add_event_detect(10, GPIO.RISING, callback=buttonpressed, bouncetime=200)
GPIO.add_event_detect(9, GPIO.RISING, callback=buttonpressed, bouncetime=200)
GPIO.add_event_detect(11, GPIO.RISING, callback=buttonpressed, bouncetime=200)
GPIO.add_event_detect(8, GPIO.RISING, callback=buttonpressed, bouncetime=200)


class LCDMenu(object):
    def __init__(self, items, dynamic_info, menu_color):
        self.position = 0
        self.items = items
        self.dynamic_info = dynamic_info
        self.menu_color = menu_color

    def navigate(self, n):
        self.position += n
        if self.position < 0:
            self.position = 0
        elif self.position >= len(self.items):
            self.position = len(self.items) - 1

    def display(self):

        global button_return
        button_return = ""

        logging.debug("--------STARTING LCD MENU--------")
        lcd.clear()
        lcd.color = self.menu_color
        time.sleep(1)

        while True:
            msg = ""
            final_msg = ""
            line_num = 1
            for index, item in enumerate(self.items):
                if self.dynamic_info != None:
                    if self.dynamic_info == 'STATUS':
                            final_msg = "Zone" + str(line_num) + ": " + common.zone_status[line_num - 1]
                    if self.dynamic_info == "SCHEDULE":
                        if self.dynamic_info:
                            if line_num == 1:
                                final_msg = "S:" + str(common.sch_start_time.strftime("%H:%M")) + " E:" + str(common.sch_end_time.strftime("%H:%M"))
                            if line_num == 2:
                                final_msg = "StartTime: " + str(common.sch_start_time.strftime("%H:%M"))
                            if line_num == 3:
                                final_msg = "EndTime: " + str(common.sch_end_time.strftime("%H:%M"))
                            if line_num == 4:
                                final_msg = " Save "

                else: 
                    final_msg = item[0]
                    lcd.color = PURPLE
                if index == self.position:   
                        msg = msg + "[-" + final_msg + "-]" + "\n"
                else:
                    msg = msg + " " + final_msg + " " + "\n"
                line_num = line_num + 1 
                lcd.message = msg
                

            if button_return == 8:
                self.items[self.position][1]()
                button_return = ""
                lcd.clear()
            elif button_return == 10:
                self.navigate(-1)
                button_return = ""
                lcd.clear()
            elif button_return == 9:
                self.navigate(1)
                button_return = ""
                lcd.clear()
            elif button_return == 11:
                break
                button_return = ""
                lcd.clear()
            time.sleep(1)
            
              
class LCDMenuApp(object):
    def __init__(self):
        
        def change_zone_status_1():
            hub.change_zone_status(1)

        def change_zone_status_2():
            hub.change_zone_status(2)

        def change_zone_status_3():
            hub.change_zone_status(3)

        def change_zone_status_4():
            hub.change_zone_status(4)

        def change_start_time():
            hub.set_schedule_start()

        def change_end_time():
            hub.set_schedule_end()

        def save_schedule_time():
            hub.save_schedule()
        
        def noop():
            pass

        time_menu_items = [
            ("Schedule ", noop),
            ("Start Time:", change_start_time),
            ("End Time:", change_end_time),
            (" Save ", save_schedule_time)
        ]
        time_menu = LCDMenu(time_menu_items, 'SCHEDULE', YELLOW)
        
        status_menu_items = [
            ("zone_status_1 ", change_zone_status_1),
            ("zone_status_2", change_zone_status_2),
            ("zone_status_3", change_zone_status_3),
            ("zone_status_4", change_zone_status_4)
        ]
        status_menu = LCDMenu(status_menu_items, 'STATUS', GREEN)
        
        main_menu_items = [
            ("Zone Readings", display_zone_readings),
            ("Zone Status", status_menu.display),
            ("Active Time", time_menu.display),
            ("Shutdown", display_shutdown)
        ]


        main_menu = LCDMenu(main_menu_items, None, PURPLE)
        main_menu.display()

# ----Zone Readings----

def display_zone_readings():
    global button_return
    button_return = ""
    lcd.clear()
    lcd.color = BLUE
    while button_return != 11:
        readings = ""
        percentage = 0
        data_list = common.real_time_moisture_readings
        lcd.message = "\n     reading data\n     please wait..."
        for obj in data_list:
           percentage =100-(((obj.moisture-400)*(100-0))/(1024-400))
           readings = readings + "Zone: " + str(obj.zone) + " | Wet: " + str(int(percentage)) +  "%\n"
        lcd.clear()
        lcd.message = readings
        time.sleep(10)
        lcd.clear()

# ----Shutdown----

def display_shutdown():
    global button_return
    
    button_return = ""
    lcd.clear()
    lcd.color = ORANGE
    while button_return != 11:
        lcd.message = "are you sure?\n o - yes\n x - no"
        if button_return == 8:
            lcd.clear()
            lcd.message = "Thank you for using:\n\nAutomated Irrigation\n      System       "
            exit()
        
def setup():
    start_lcd = LCDMenuApp()
