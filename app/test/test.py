import time
import sys
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)

# Setup Inputs
GPIO.setup(10, GPIO.IN, pull_up_down=GPIO.PUD_UP) # SW1
GPIO.setup(9, GPIO.IN, pull_up_down=GPIO.PUD_UP) # SW2
GPIO.setup(11, GPIO.IN, pull_up_down=GPIO.PUD_UP) # SW3
GPIO.setup(8, GPIO.IN, pull_up_down=GPIO.PUD_UP) # SW4

# Setup Outputs 
GPIO.setup(5, GPIO.OUT)
GPIO.setup(6, GPIO.OUT)
GPIO.setup(16, GPIO.OUT)
GPIO.setup(17, GPIO.OUT)

def my_callback_1(channel):
    print('Callback 1')

def my_callback_2(channel):
    print('Callback 2')

def my_callback_3(channel):
    print('Callback 3')

def my_callback_4(channel):
    print('Callback 4')


GPIO.add_event_detect(10, GPIO.RISING, callback=my_callback_1, bouncetime=200)
GPIO.add_event_detect(9, GPIO.RISING, callback=my_callback_2, bouncetime=200)
GPIO.add_event_detect(11, GPIO.RISING, callback=my_callback_3, bouncetime=200)
GPIO.add_event_detect(8, GPIO.RISING, callback=my_callback_4, bouncetime=200)


def test_hub_buttons():
    while True:
    selection = raw_input("Q: Quit")
    if selection is "Q" or selection is "q":
        print("Quitting")
        sys.exit()


def test_hub_relays():
    #Terminal Out 1
    GPIO.output(5, 1)
    time.sleep(3)
    GPIO.output(5, 0)

    #Terminal Out 2 
    GPIO.output(6, 1)
    time.sleep(3)
    GPIO.output(6, 0) 


    #Terminal Out 3 
    GPIO.output(16, 1)
    time.sleep(3)
    GPIO.output(16, 0) 

    #Terminal Out 4
    GPIO.output(17, 1)
    time.sleep(3)
    GPIO.output(17, 0) 

    GPIO.cleanup()








