import time
import sys
import pretty_errors
import datetime
import logging
from pprint import pprint

from xbee import coordinator
from common import *

formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(name)s:%(message)s')


def setup_logger(name, log_file, level=logging.INFO):
    """To setup as many loggers as you want"""

    handler = logging.FileHandler(log_file)        
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger

# logging.basicConfig(filename='xbee_test.log', level=logging.DEBUG, format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')

xbee_test_logger = setup_logger("xbee_test_logger", "xbee_test.log")

def xbee_test():
    xbee_test_logger.info('--------START OF TESTING APPLICATION--------')
    print("--------START OF TESTING APPLICATION--------\n")
    common.init()
    coordinator.xbee_network_init()
    xbee_test_logger.info('--------Xbee Network Initialized--------')
    print("--------Xbee Network Initialized--------\n")
    start_time = time.time()
    xbee_test_logger.info("Test Loop Started at: " + str(datetime.datetime.fromtimestamp(start_time)))
    print("Test Loop Started at: " + str(datetime.datetime.fromtimestamp(start_time)) + "\n")
    

    while True:
        try:
            data = coordinator.get_all_zone_readings()
            for obj in data:
                xbee_test_logger.info("ZONE: " + str(obj.zone) + " READING: " + str(obj.moisture))
                print("ZONE: " + str(obj.zone) + " READING: " + str(obj.moisture))
            print("\n")
            time.sleep(300) # Check every 5 minutes
        except:
             xbee_test_logger.info('error returned')
             print("error returned\n")
             continue
        #finally:
        #    print("\n")
    
    xbee_test_logger.info("Test loop ended at: " + str(time.time()))
    print("Test loop ended at: " + str(time.time() + "\n"))
    total_time = start_time - time.time()
    xbee_test_logger.info("Total Time of Application: " + str(total_time))
    print("Total Time of Application: " + str(total_time) + "\n")
    xbee_test_logger.info('--------END OF TESTING APPLICATION--------')
    print("--------END OF TESTING APPLICATION--------/n")

