import os

dependencies = 'webpack webpack-cli html-webpack-plugin '\
        'mini-css-extract-plugin carbon-web-components '\
        'sass carbon-components css-loader sass-loader lit-html lit-element '\
        '@carbon/charts @carbon/themes d3'

def build_clent():
    '''Build the Web Client.'''
    os.system('npm i --save-dev ' + dependencies)

    os.system('npx webpack build -c ./client/webpack.config.js')