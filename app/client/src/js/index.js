import 'carbon-web-components/es/components/ui-shell/header';
import 'carbon-web-components/es/components/ui-shell/header-name';
import 'carbon-web-components/es/components/ui-shell/header-menu';
import 'carbon-web-components/es/components/ui-shell/header-nav';
import 'carbon-web-components/es/components/ui-shell/header-nav-item';
import 'carbon-web-components/es/components/ui-shell/side-nav';
import 'carbon-web-components/es/components/ui-shell/side-nav-menu';
import 'carbon-web-components/es/components/ui-shell/side-nav-items';
import 'carbon-web-components/es/components/ui-shell/side-nav-menu-item';
import 'carbon-web-components/es/components/ui-shell/side-nav-link';
import 'carbon-web-components/es/components/ui-shell/header-menu-button';
import 'carbon-web-components/es/components/dropdown/dropdown';
import 'carbon-web-components/es/components/dropdown/dropdown-item';
import 'carbon-web-components/es/components/data-table/table-body';
import 'carbon-web-components/es/components/data-table/table';
import 'carbon-web-components/es/components/data-table/table-cell';
import 'carbon-web-components/es/components/data-table/table-head';
import 'carbon-web-components/es/components/data-table/table-row';
import 'carbon-web-components/es/components/data-table/table-header-cell';
import 'carbon-web-components/es/components/data-table/table-header-row';
import 'carbon-web-components/es/components/data-table/table-toolbar-content';
import 'carbon-web-components/es/components/data-table/table-toolbar';
import 'carbon-web-components/es/components/data-table/table-batch-actions';
import 'carbon-web-components/es/components/pagination/pagination';
import 'carbon-web-components/es/components/button/button';
import { LineChart } from "@carbon/charts";


import "../style/index.scss";
import listReadings from "../js/list-readings";

const settingsTemp = {
    render: () => {
        return `
      <div class="bx--row">
      
      <div class="controls bx--col">
         <h1>Settings</h1>
        <div id="dropdown-container"></div>
        <bx-btn id='run-now'>Initialize</bx-btn>
      </div>
      </div>
      
      <div class="bx--row">
      <div class="bx--col" id="table-container"></div>
      
      </div>`;
    },
    state: 'settings',
}

const homeTemp = {
    render: () => {
        return `
      <div class="bx--row">
      
      <div class="controls bx--col">
         <h1>Welcome</h1>
      </div>
      </div>
      
      </div>`;
    },
    state: 'settings',
}

const routes = [
    { path: '/settings', component: settingsTemp, },
    { path: '/zones', component: listReadings, },
    { path: '/', component: homeTemp, },
];

var websocket = new WebSocket("ws://127.0.0.1:5678/");

const parseLocation = () => location.hash.slice(1).toLowerCase() || '/';
const findComponentByPath = (path, routes) => routes.find(r => r.path.match(new RegExp(`^\\${path}$`, 'gm'))) || undefined;

const router = () => {
    // Find the component based on the current path
    const path = parseLocation();
    // If there's no matching route, get the "Error" component
    const { component = ErrorComponent } = findComponentByPath(path, routes) || {};
    // Render the component in the "app" placeholder
    document.getElementById('app').innerHTML = component.render();
    console.log(component.state)
    viewController(component.state);
};



// Grab chart holder HTML element and initialize the chart
function createChart(data){
    const options = {
        data: {
            groupMapsTo: "zone"
        },
        "title": "Zone Readings",
        "axes": {
            "bottom": {
                "title": "Date",
                "mapsTo": "timeStamp",
                "scaleType": "time"
            },
            "left": {
                "mapsTo": "moisture",
                "title": "Mositure",
                "scaleType": "linear"
            }
        },
        "curve": "curveMonotoneX",
        "height": "400px"
    };

    const chartHolder = document.getElementById("chart-container");
    new LineChart(chartHolder, {
	    data,
	    options
    });
}



window.addEventListener('hashchange', router);
window.addEventListener('load', router);




function viewController(appState) {

    switch (appState) {
        case "listzones":
            buildZoneList();
            var initBtn = document.getElementById('run-now');
            initBtn.onclick = getZone;
            break;
        default:

            break;

    }
}

function getZone(event) {
    console.log("getZone Running");
    var x = document.getElementById("zone-selector").value;
    websocket.send(JSON.stringify({ zone: x }));
}

function buildZoneList() {

    const zoneList = [1, 2, 3, 4];
    var drpDown = document.createElement("bx-dropdown");
    drpDown.addEventListener('change', function () { getZone() });
    drpDown.triggerContent = "Select a zone";
    drpDown.id = "zone-selector";
    drpDown.setAttribute('color-scheme', 'light');
    zoneList.forEach(createList);

    function createList(value, index, array) {
        var option = document.createElement("bx-dropdown-item");
        option.autofocus = true;
        option.value = value;
        option.innerHTML = "Zone " + value;
        drpDown.appendChild(option);
    }


    document.getElementById("dropdown-container").appendChild(drpDown);
}

websocket.onmessage = function (event) {
    console.log("Recieving Data From Hub.");
    createChart(JSON.parse(event.data));
    //createTable(JSON.parse(event.data));

};

// Create table
function createTable(data) {
    console.log("Creating Table");

    // Check if at least one value exists in the json array
    if (data.length > 0 && Array.isArray(data)) {

        // Get div to create table
        var mytable = document.getElementById("table-container");

        // Clear table if exists
        var t = document.getElementById("freshTable");
        if (t) {
            t.remove();
        }

        // Create table 
        var table = document.createElement('bx-table');
        table.setAttribute('id', 'freshTable');
        table.setAttribute("border", "1");
        table.style.borderCollapse = "collapse";
        table.appendChild(createTHead(data));
        table.appendChild(createTBody(data));
        mytable.appendChild(table);
    }
}

// Create table head
function createTHead(data) {
    console.log("Creating Table Head");
    var keys = Object.keys(data[0]);
    var thead = document.createElement('bx-table-head');
    var tr = document.createElement('bx-table-header-row');
    for (var col = 0; col < keys.length; col++) {
        var th = document.createElement('bx-table-header-cell');
        th.appendChild(document.createTextNode(keys[col]));
        tr.appendChild(th);
    }
    thead.appendChild(tr);
    return thead;
}

// Create table body
function createTBody(data) {
    console.log("Creating Table Body");
    var keys = Object.keys(data[0]);
    var tbody = document.createElement('bx-table-body');
    tbody.setAttribute('color-scheme', 'zebra');
    for (var row = 0; row < data.length; row++) {
        var tr = document.createElement('bx-table-row');
        for (var col = 0; col < keys.length; col++) {
            var td = document.createElement('bx-table-cell');
            td.appendChild(document.createTextNode(data[row][keys[col]]));
            tr.appendChild(td);
        }
        tbody.appendChild(tr);
    }
    return tbody;
}
