// const listReadings = {
//     render: () => {
//         return `
//       <div class="bx--row">
      
//       <div class="controls bx--col">
//          <h1>Zones</h1>
//         <div id="dropdown-container"></div>
//         <bx-btn id='run-now'>Initialize</bx-btn>
//       </div>
//       </div>
      
//       <div class="bx--row">
//       <div class="bx--col" id="table-container"></div>
      
//       </div>`;
//     },
//     state: 'listzones',
// }

const listReadings = {
  render: () => {
      return `
    <div class="bx--row">
    
    <div class="controls bx--col">
       <h1>Zones</h1>
      <div id="dropdown-container"></div>
      <bx-btn id='run-now'>Initialize</bx-btn>
    </div>
    </div>
    
    <div class="bx--row">
    <div class="bx--col" id="chart-container"></div>
    
    </div>`;
  },
  state: 'listzones',
}

export default listReadings;