import asyncio
import datetime
import time
import websockets
import json
import socket
import threading
import logging
import os
import signal


from common import *
from db import database
from console_args.c_args import CONSOLE_ARGS as args


async def ws_zone(websocket, path):
    async for message in websocket:
        data = json.loads(message)
        #print(message)
        zone_numb = data["zone"]
        get_all_zone = database.get_reading_by_zone_json(common.conn, zone_numb)
        readingData = json.dumps(get_all_zone)
        #print(readingData)
        await websocket.send(readingData)


def ask_exit(signame, loop):
    print("got signal %s: exit" % signame)
    loop.stop()


async def create_ws_server():

    if args.local:
        local_ip = common.DEV_IP_ADDR
    else:
        local_ip = common.get_ip()

    async with websockets.serve(
        ws_zone,
        local_ip,
        port=5678,
        reuse_port=True,
    ):
        await asyncio.Future()

#        except asyncio.CancelledError:
#            logging.debug("Task Canceled. Web Socket Server Stopped.")