"""
parsing of command line arguments.

No other functionality should be added to this module.
The typically usage is:

>>> from console_args import CONSOLE_ARGS 
"""

import argparse

def _parse_arguments():

    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--automate", help="Automate Scheduler, initialize hub logic.", action="store_true", dest="automate")
    parser.add_argument("-b", "--build-client", help="Build Web Client.", action="store_true", dest="client")
    parser.add_argument("-c", "--create-table", help="Create new readings table.", action="store_true", dest="create")
    parser.add_argument("-d", "--development", help="Run in development mode.", action="store_true", dest="dev")
    parser.add_argument("-f", "--fake-readings", help="Insert random fake moisture readings data.", action="store_true", dest="fake")
    parser.add_argument("-i", "--local-ip", help="Use local IP for web server.", action="store_true", dest="local")
    parser.add_argument("-l", "--run-lcd", help="shows lcd menu.", action="store_true", dest="lcd")
    parser.add_argument("-m", "--shell-menu", help="Run the curses shell menu.", action="store_true", dest="shell")
    parser.add_argument("-r", "--real-time-data", help="Enable realtime data from sensors.", action="store_true", dest="real_time")
    parser.add_argument("-s", "--start-server", help="Start server.", action="store_true", dest="server")
    parser.add_argument("-x", "--xbee-network-init", help="Initialize Xbee network.", action="store_true", dest="xbee")
    parser.add_argument("-t", "--test-xbee-sensor", help="Test Xbee sensor.", action="store_true", dest="test")

    return parser.parse_args()

CONSOLE_ARGS =  _parse_arguments()
