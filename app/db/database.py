import logging
import sqlite3
from sqlite3 import Error
from common.readings import MoistureReadings


def create_connection(db_file):
    """Creates a database connection object to the SQLite database """
    conn = None
    try:
        conn = sqlite3.connect(db_file, check_same_thread=False) #to put database in memory for testing use :memory:
        print("Database connection created.")
    except Error as e:
        print(e)

    return conn

def create_table(conn):
    '''Creates a table for readings within the sqlite database if no table exsists'''
    cursor = conn.cursor()
    with conn:
        cursor.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='readings' ''')
        if cursor.fetchone()[0]==1: 
	        logging.info('Table already exists.')
        
        else:
            logging.info("Table does not exsist. Creating a new tables.")
            cursor.execute("""CREATE TABLE IF NOT EXISTS readings (
                timeStamp text,
                zone integer,
                moisture real
                )""")
            cursor.execute("""CREATE TABLE IF NOT EXISTS schedules (
                zone integer,
                timeStart text,
                timeEnd text
                )""")
        
    conn.commit()

def insert_reading(conn, reading):
    '''Insert row into readings table, takes in moisture reading and puts it into the database'''
    cursor = conn.cursor()
    with conn:
        cursor.execute("INSERT INTO readings VALUES (:timeStamp, :zone, :moisture)", {'timeStamp': reading.timeStamp, 'zone': reading.zone, 'moisture': reading.moisture})

def get_reading_by_zone(conn, zone_num):
    '''Returns all moisture readings in the database from one particular zone'''
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM readings WHERE zone=:zone", {'zone': zone_num})
    return cursor.fetchall()

def get_last_reading_by_zone(conn, zone_num):
    '''Returns all moisture readings in the database from one particular zone'''
    cursor = conn.cursor()
    cursor.execute("SELECT moisture, zone=:zone FROM readings LIMIT 1", {'zone': zone_num})
    return cursor.fetchall()

def get_reading_by_zone_json(conn, zone_num):
    '''Returns all moisture readings in the database from one particular zone'''
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM readings WHERE zone=:zone ORDER BY timeStamp", {'zone': zone_num})
    r = [dict((cursor.description[i][0], value) \
               for i, value in enumerate(row)) for row in cursor.fetchall()]
    return r

def remove_reading(conn, reading):
    '''Delets a reading from database acording to timeStamp and Zone '''
    cursor = conn.cursor()
    with conn:
        cursor.execute("DELETE from readings WHERE timeStamp = :time AND zone = :zone",
                  {'timeStamp': reading.timeStamp, 'zone': reading.zone})

def insert_schedule(conn , schedule):
    '''Insert Schedule into schedule table'''
    cursor = conn.cursor()
    with conn:
        cursor.execute("INSERT INTO schedules VALUES (:zone, :timeStart, :timeEnd)", {'zone': schedule.zone, 'timeStart': schedule.timeStart, 'timeEnd': schedule.timeEnd})

def get_schedule_by_zone(conn, zone_num):
    '''Returns schedule from one particular zone'''
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM schedules WHERE zone=:zone", {'zone': zone_num})
    return cursor.fetchall()

def delete_dulpicate_schedule(conn):
    cursor = conn.cursor()
    cursor.execute("DELETE FROM schedules WHERE rowid NOT IN (SELECT min(rowid) FROM schedules GROUP BY zone, timeStart, timeEnd) ")
