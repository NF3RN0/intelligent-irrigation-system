import requests
import pretty_errors
import json

api_key = ""

lat = "32.98" 
log = "-96.7"
excl = "minutely,daily"
url = "https://api.openweathermap.org/data/2.5/onecall?lat=" + lat + "&lon=" + log + "&exclude=" + excl + "&appid=" + api_key

def kelvinToFahrenheit(kelvin):
    return kelvin * 1.8 - 459.67

def get_current_temperature(url):
    '''Gets the current temperature from the Open Weather Maps API and returns it in Fahrenheit'''
    response = requests.get(url)
    dataRecieved = response.json()
    currentTempKelvin = dataRecieved["current"]["temp"]
    return kelvinToFahrenheit(currentTempKelvin) 
 
