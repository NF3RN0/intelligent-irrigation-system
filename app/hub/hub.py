import time
import datetime
from datetime import timedelta
import sys
import sched
import threading
import pretty_errors
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

from common import *
from weather import api
from db import database
from wss import wss_server
from console_args.c_args import CONSOLE_ARGS as args
from xbee import coordinator
from common.schedule import IrrigationSchedule
from common.readings import MoistureReadings

currentDateTime = datetime.datetime.now()

if not (args.dev or args.create or args.fake):
    import RPi.GPIO as GPIO

    GPIO.setmode(GPIO.BCM)

    GPIO.setwarnings(False)

    # Setup Outputs 
    GPIO.setup(5, GPIO.OUT) # Relay 1
    GPIO.setup(6, GPIO.OUT) # Relay 2
    GPIO.setup(16, GPIO.OUT) # Relay 3
    GPIO.setup(17, GPIO.OUT) # Relay 4

    GPIO.output(5, 0)
    GPIO.output(6, 0)
    GPIO.output(16, 0)
    GPIO.output(17, 0)



zone_pins = [5 , 6 , 16 , 17]

class DatabaseChangeHandler(FileSystemEventHandler):
    def on_modified(self, event):
        start_schedule()
    
def get_all_zones_real_time():
    while common.enable_real_time_data == True:
        if args.dev:
            common.real_time_moisture_readings = common.create_random_xbee_data()
        else:
            common.real_time_moisture_readings = coordinator.get_all_zone_readings()
        for obj in common.real_time_moisture_readings:
            reading_now = obj
            database.insert_reading(common.conn, reading_now) 
        time.sleep(300)

def change_zone_status(zone):
    debug.logging("Changing zone: " + str(zone))
    current_zone_status = common.zone_status[zone -1]
    if current_zone_status == "ON":
        common.zone_status[zone - 1] = "OFF"
        GPIO.output(zone_pins[zone - 1], 0)
    if current_zone_status == "OFF":
        common.zone_status[zone - 1 ] = "ON"
        GPIO.output(zone_pins[zone - 1], 1)

def get_zone_status(pin):
    status = GPIO.input(pin)
    return status

def set_schedule_start():
    current_start_time = common.sch_start_time
    time_change = datetime.timedelta(minutes=15)
    new_time = current_start_time + time_change
    common.sch_start_time = new_time

def set_schedule_end():
    current_end_time  = common.sch_end_time
    time_change = datetime.timedelta(minutes=15)
    new_time = current_end_time + time_change
    common.sch_end_time = new_time

def save_schedule():
    zone_num = 1
    for obj in zone_pins:
        database.insert_schedule(common.conn, IrrigationSchedule(zone_num, common.sch_start_time, common.sch_end_time) )
        database.delete_dulpicate_schedule(common.conn)
        logging.debug(database.get_schedule_by_zone(common.conn, zone_num))
        zone_num = zone_num + 1
    

def init_schedule():
    '''Takes schedule per zone from database and creates scheduled task. Needs to be threaded.'''
    zone_num = 1
    for obj in zone_pins:
        data_returned = database.get_schedule_by_zone(common.conn, zone_num)
        for obj_ in data_returned:
            sch = sched.scheduler(time.time,time.sleep)
            convert_start = datetime.datetime.strptime(obj_[1], '%Y-%m-%d %H:%M:%S')
            convert_end = datetime.datetime.strptime(obj_[2], '%Y-%m-%d %H:%M:%S')
            ts = time.mktime(convert_start.timetuple()) + convert_start.microsecond/1e6
            run_time = convert_start - convert_end
            sch.enterabs(ts, 1, automation, argument=(zone_num, run_time) )
        zone_num = zone_num + 1
    logging.debug(sch.queue)
    sch.run()
    return sch

def start_schedule():
    '''Thread function for automated scheduler. Creates a thread for init_schedule'''
    print("Starting Schedule Thread")
    global sch_t
    sch_t = threading.Thread(target=init_schedule, args=())
    sch_t.start()

def automation(zone, run_time):
    '''Function to initialize automation process.
    Checks system wide conditions to determine if the irrigation process starts.'''

    logging.debug("----Checking Conditions----")
    currentTemp = api.get_current_temperature(api.url)
    logging.debug("Current Temperature: " + currentTemp)
    if currentTemp <= 32:
        logging.debug("FREEZE WARNING: Current Temperature is below threshold for irrigation! Automation process halted!")
    else:
        logging.debug("Real Time Readings for all zones: ")
        logging.debug(common.real_time_moisture_readings)
        current_reading = common.real_time_moisture_readings [zone - 1]
        current_value = current_reading.moisture
        logging.debug("Real Time Readings for " + zone + ":" + current_value)
        if current_value >= 700:
            change_zone_status(zone)
            time.sleep(15)
            change_zone_status(zone)
        else:
            logging.debug("Irrigation for " + zone + " not needed.")

